# README #

This is a test app for DotNet CORE 2.2

### What is this repository for? ###

* Quick DotNet Deployment Demo

### How do I get set up? ###

* AWS - Create ECS 'dotnetTestApp'
* AWS - Create new IAM user 'BitbucketPipelines' and attach policies 'ecs:UpdateService' and 'ecs:RegisterTaskDefinition'
* AWS - Create ECS Service 'dotnetTestApp-STAGING' as Fargate in namespace 'staging'
* AWS - Create ECS Service 'dotnetTestApp-PROD' as Fargate in namespace 'production'

* Build (Local):

    ```
    docker build -t dotnet_test_app:latest .
    ```

* Run (Local):

    ```
    docker run -d -p 80:80 --name dotnet_test_app dotnet_test_app
    ```

* Build (Remote):

    ```
    git checkout build;
    git merge master;
    git push;
    ```


### How do I deploy? ###

* Deploy (STAGING): 

    ```
    git checkout deploy-staging;
    git merge master;
    git commit -a -m 'Deploy STAGING';
    git push
    ```

* Deploy (PROD): 

    ```
    git checkout deploy-prod;
    git merge master;
    git commit -a -m 'Deploy PROD';
    git push
    ```
